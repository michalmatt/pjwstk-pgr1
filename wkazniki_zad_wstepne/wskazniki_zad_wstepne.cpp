#include <iostream>
#include <conio.h>

using namespace std;

int main() {
	//zad1
	int n = 3;
	int *wsk = &n;

	//zad2
	cout << "Co pod wskaznikiem: " << *wsk << endl;
	cout << "Na co wskaznik: " << wsk << endl;

	//zad3
	int *wskInt2 = wsk;

	//zad4
	//float *wskFloat = wsk;	//blad, dlatego, ze brak rzutowania (inny typ wskaznika)

	//zad5
	void *wskVoid = wsk;		//brak bledu, bo do void mozna przypisac wskaznik kazdego typu

								//zad6
								//char *wskChar = wsk;

								//zad7
	int A[10];
	int *wskA;
	wskA = A;			//sposob1
	wskA = &A[0];		//sposob2

						//zad8
	wskA = &A[4];

	//zad9
	wskA + 3;

	//zad10
	wskA = A;
	*wskA = 6;
	cout << A[0] << endl;

	//zad11
	*(wskA + 5) = 6;
	cout << A[5] << endl;

	//zad12
	int tablica[20];
	int *wsk1 = &tablica[5];
	int *wsk2 = &tablica[14];
	cout << wsk2 - wsk1 << endl;		//pokazuje po prostu odkleglosc pomiedzy dwoma wskaznikami

										//zad13
	const float *wskDoObiektuStalego;
	//*wskDoObiektuStalego = 1;			//przyklad niedozwolonej oprecji: proba zmiany wartosci

	//zad14
	float x;
	float y;
	float * const wskStaly = &x;
	//wskStaly = &y;					//przyklad niedozwolonej operacji: proba zmiany na co wskazuje wskaznik

	//zad15
	const float * const wskStalyDoObiektuStalego = &y;

	_getch();
	return EXIT_SUCCESS;
}