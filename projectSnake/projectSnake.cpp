#include <iostream>
#include <conio.h>
#include <ctime>
#include <windows.h>

using namespace std;

#define STRZALKA_W_GORE 72
#define STRZALKA_W_DOL 80
#define STRZALKA_W_LEWO 75
#define STRZALKA_W_PRAWO 77
#define ESC 27
#define ENTER 13
#define NOWA_GRA 1
#define WYJDZ_Z_GRY 2

const int PLANSZA_LICZBA_KOLUMN_X = 20;
const int PLANSZA_LICZBA_WIERSZY_Y = 20;
const int OBSZAR_GRY_PIERWSZY_INDEX_XY = 1;
const int OBSZAR_GRY_OSTATNI_INDEX_X = PLANSZA_LICZBA_KOLUMN_X - 2;
const int OBSZAR_GRY_OSTATNI_INDEX_Y = PLANSZA_LICZBA_WIERSZY_Y - 2;

enum TypeKierunek { GORA, DOL, LEWO, PRAWO };

//zmienne globalne
TypeKierunek kierunekPoprzedni = GORA;
int pozycjaOwocX;
int pozycjaOwocY;
int dlugoscWeza = 3;
int liczbaPunktow = 0;
bool koniecGry;
int pozycjaWeza[PLANSZA_LICZBA_WIERSZY_Y * PLANSZA_LICZBA_KOLUMN_X];

void infoNadPlansza() {
	cout << "ESC = wyjscie do MENU" << endl;
	cout << "ENTER = pauza" << endl;
	cout << "Liczba punktow: " << liczbaPunktow << endl;
}

void rysujPlansze() {
	system("cls");
	infoNadPlansza();
	char Plansza[PLANSZA_LICZBA_WIERSZY_Y][PLANSZA_LICZBA_KOLUMN_X];
	int i;
	int j;
	for (i = 0; i < PLANSZA_LICZBA_WIERSZY_Y; i++) {
		for (j = 0; j < PLANSZA_LICZBA_KOLUMN_X; j++) {
			if (i == 0) {
				Plansza[i][j] = '#'; //znak gornej krawedzi planszy
			}
			else if (i == PLANSZA_LICZBA_WIERSZY_Y - 1) {
				Plansza[i][j] = '#'; //znak dolnej krawedzi planszy
			}
			else if (j == 0 || j == PLANSZA_LICZBA_KOLUMN_X - 1) {
				Plansza[i][j] = '#'; //znak bocznych krawedzi planszy
			}
			else if (i == pozycjaOwocY && j == pozycjaOwocX) {
				Plansza[pozycjaOwocY][pozycjaOwocX] = 'o';
			}
			else {
				Plansza[i][j] = ' ';
			}
			int a;
			for (a = 0; a < dlugoscWeza * 2; a = a + 2) {
				if (i == pozycjaWeza[a] && j == pozycjaWeza[a + 1]) {
					Plansza[i][j] = 'x';
				}
			}
		}
	}
	for (i = 0; i < PLANSZA_LICZBA_WIERSZY_Y; i++) {
		for (j = 0; j < PLANSZA_LICZBA_KOLUMN_X; j++) {
			cout << Plansza[i][j];
		}
		cout << endl;
	}
}

void rysujPlanszeGameOver() {
	system("cls");
	infoNadPlansza();
	char Plansza[PLANSZA_LICZBA_WIERSZY_Y][PLANSZA_LICZBA_KOLUMN_X];
	int i;
	int j;
	for (i = 0; i < PLANSZA_LICZBA_WIERSZY_Y; i++) {
		for (j = 0; j < PLANSZA_LICZBA_KOLUMN_X; j++) {
			if (i == 0) {
				Plansza[i][j] = '#'; //znak gornej krawedzi planszy
			}
			else if (i == PLANSZA_LICZBA_WIERSZY_Y - 1) {
				Plansza[i][j] = '#'; //znak dolnej krawedzi planszy
			}
			else if (j == 0 || j == PLANSZA_LICZBA_KOLUMN_X - 1) {
				Plansza[i][j] = '#'; //znak bocznych krawedzi planszy
			}
			else if ((i == PLANSZA_LICZBA_WIERSZY_Y / 2 + 1 || i == PLANSZA_LICZBA_WIERSZY_Y / 2 - 1) && j <= PLANSZA_LICZBA_KOLUMN_X / 2 + 5 && j >= PLANSZA_LICZBA_KOLUMN_X / 2 - 5) {
				Plansza[i][j] = '*';
			}
			else {
				Plansza[i][j] = ' ';
			}
			char GameOver[] = { 'G', 'A', 'M', 'E',' ', 'O', 'V', 'E', 'R' };
			int a;
			int punktPoczatkowyNapisu = PLANSZA_LICZBA_KOLUMN_X / 2 - 4;
			for (a = 0; a < 9; a++, punktPoczatkowyNapisu++) {
				if (i == PLANSZA_LICZBA_WIERSZY_Y / 2 && j == punktPoczatkowyNapisu) {
					Plansza[i][j] = GameOver[a];
				}
			}
		}
	}
	for (i = 0; i < PLANSZA_LICZBA_WIERSZY_Y; i++) {
		for (j = 0; j < PLANSZA_LICZBA_KOLUMN_X; j++) {
			cout << Plansza[i][j];
		}
		cout << endl;
	}
}

bool czySciana() {
	if (pozycjaWeza[1] == 0 || pozycjaWeza[1] == PLANSZA_LICZBA_KOLUMN_X - 1 || pozycjaWeza[0] == 0 || pozycjaWeza[0] == PLANSZA_LICZBA_WIERSZY_Y - 1) {
		return true;
	}
	return false;
}

bool czyOgon() {
	int i;
	for (i = 2; i < dlugoscWeza * 2; i = i + 2) {
		if (pozycjaWeza[0] == pozycjaWeza[i] && pozycjaWeza[1] == pozycjaWeza[i + 1]) {
			return true;
		}
	}
	return false;
}

bool czyOwoc() {
	if (pozycjaWeza[1] == pozycjaOwocX && pozycjaWeza[0] == pozycjaOwocY) {
		return true;
	}
	return false;
}

void generujOwoc() {
	srand(time(NULL));
	pozycjaOwocX = rand() % (OBSZAR_GRY_OSTATNI_INDEX_X - OBSZAR_GRY_PIERWSZY_INDEX_XY + 1) + OBSZAR_GRY_PIERWSZY_INDEX_XY;
	pozycjaOwocY = rand() % (OBSZAR_GRY_OSTATNI_INDEX_Y - OBSZAR_GRY_PIERWSZY_INDEX_XY + 1) + OBSZAR_GRY_PIERWSZY_INDEX_XY;
}

void czyNacisnietyPrzycisk() {
	if (_kbhit()) {
		unsigned char przycisk = _getch();
		switch (przycisk) {
		case STRZALKA_W_GORE:
		{
			if (kierunekPoprzedni != DOL) {
				kierunekPoprzedni = GORA;
			}
			break;
		}
		case STRZALKA_W_DOL:
		{
			if (kierunekPoprzedni != GORA) {
				kierunekPoprzedni = DOL;
			}
			break;
		}
		case STRZALKA_W_LEWO:
		{
			if (kierunekPoprzedni != PRAWO) {
				kierunekPoprzedni = LEWO;
			}
			break;
		}
		case STRZALKA_W_PRAWO:
		{
			if (kierunekPoprzedni != LEWO) {
				kierunekPoprzedni = PRAWO;
			}
			break;
		}
		case ENTER:
		{
			while (true) {
				if (_getch() == ENTER) {
					break;
				}
			}
			break;
		}
		case ESC:
		{
			koniecGry = true;
			break;
		}
		}
	}
}

void sprawdzenieNowejPozycji() {
	if (czyOwoc() == true) {
		pozycjaWeza[dlugoscWeza * 2] = pozycjaOwocY;
		pozycjaWeza[dlugoscWeza * 2 + 1] = pozycjaOwocX;
		liczbaPunktow++;
		dlugoscWeza++;
		generujOwoc();
		rysujPlansze();
	}
	else if (czySciana() == true || czyOgon() == true) {
		rysujPlanszeGameOver();
		koniecGry = true;
	}
	else {
		czyNacisnietyPrzycisk();
		rysujPlansze();
	}
}

void startowaPozycjaWeza() {
	int i;
	int odlegloscOdScianyPrawej = 4;
	for (i = 1; i < dlugoscWeza * 2; i = i + 2) {
		pozycjaWeza[i] = OBSZAR_GRY_OSTATNI_INDEX_X - odlegloscOdScianyPrawej;
	}
	int odlegloscOdScianyDolnej = 4;
	for (i = 0; i < dlugoscWeza * 2; i = i + 2, odlegloscOdScianyDolnej--) {
		pozycjaWeza[i] = OBSZAR_GRY_OSTATNI_INDEX_Y - odlegloscOdScianyDolnej;
	}
}

void rysujWeza() {
	int i;
	for (i = 1; i <= dlugoscWeza * 2 - 2; i++) {
		pozycjaWeza[dlugoscWeza * 2 - i] = pozycjaWeza[dlugoscWeza * 2 - i - 2];
	}
}

int main() {
	while (true) {
		cout << "Witamy w grze SNAKE." << endl;
		cout << "Wpisz numer pozycji z MENU ponizej i nacisnij ENTER.\n(1) Nowa gra\n(2) Wyjdz z gry" << endl;
		int wybor;
		cin >> wybor;
		switch (wybor) {
		case NOWA_GRA:
		{
			generujOwoc();
			startowaPozycjaWeza();
			rysujPlansze();

			koniecGry = false;
			while (koniecGry == false) {
				switch (kierunekPoprzedni) {
				case GORA:
				{
					rysujWeza();
					pozycjaWeza[0] -= 1;
					sprawdzenieNowejPozycji();
				}
				break;
				case DOL:
				{
					rysujWeza();
					pozycjaWeza[0] += 1;
					sprawdzenieNowejPozycji();
				}
				break;
				case LEWO:
				{
					rysujWeza();
					pozycjaWeza[1] -= 1;
					sprawdzenieNowejPozycji();
				}
				break;
				case PRAWO:
				{
					rysujWeza();
					pozycjaWeza[1] += 1;
					sprawdzenieNowejPozycji();
				}
				break;
				}
				Sleep(500);
			}
			continue;
		}

		case WYJDZ_Z_GRY:
		{
			cout << "Nacisnij ENTER, aby potwierdzic wyjscie z gry SNAKE...";
			_getch();
			return EXIT_SUCCESS;
			break;
		}

		default:
		{
			cout << "Nie ma takiej pozycji. Nacisnij ENTER, aby wybrac ponownie.";
			_getch();
			system("cls");
			continue;
		}

		}
	}
}