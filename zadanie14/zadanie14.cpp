#define _USE_MATH_DEFINES
#include <iostream>
#include <conio.h>
#include <math.h>

using namespace std;

const int A = 0;
const int B = 1;

int main() {
	while (1) {
		cout << "Witamy w programie obliczajacym pierwiastek funkcji f(x) w przedziale <a,b> metoda bisekcji, gdzie:" << endl;
		cout << " f(x) = e^(-1.5x) - 0.3x^2\n a = 0;\n b = 1;" << endl;
		cout << "Wybierz jedna z ponizszych opcji, aby kontynuowac." << endl;
		cout << "(1) Rozpocznij\n(2) Zakoncz" << endl;

		int wybor;
		cin >> wybor;

		if (wybor == 1 || wybor == 2) {

			switch (wybor) {
			case 1:
			{
				double epsilon;
				cout << "Podaj dokladnosc (Epsilon) z jaka chcesz obliczyc pierwiastek.\nNp. 0.0000001" << endl;
				do {
					cout << "Epsilon = "; cin >> epsilon;
					if (epsilon < 0 || epsilon > 1) {
						cout << "Wartosc Epsilon nie moze byc mniejsza od 0 lub wieksza od 1. Podaj ja ponownie." << endl;
					}
				} while (epsilon < 0 || epsilon > 1);

				double wartoscA = pow(M_E, -1.5*A) - 0.3 * pow(A, 2);
				double wartoscB = pow(M_E, -1.5*B) - 0.3 * pow(B, 2);
				double pierw;

				if (wartoscA == 0) {
					pierw = A;
					cout << "\nPierwiastek funkcji f(x) = e^(-1.5x)-0.3x^2 w przedziale <0,1> obliczony z dokladnoscia " << epsilon << " wynosi: " << pierw << endl;
					cout << "Liczba  iteracji: N/A" << endl;
				}
				else if (wartoscB == 0) {
					pierw = B;
					cout << "\nPierwiastek funkcji f(x) = e^(-1.5x)-0.3x^2 w przedziale <0,1> obliczony z dokladnoscia " << epsilon << " wynosi: " << pierw << endl;
					cout << "Liczba  iteracji: N/A" << endl;
				}
				else {
					double a = A;
					double b = B;
					double c;
					int i;
					for (i = 0; abs(a - b) >= epsilon; i++) {
						c = (a + b) / 2;
						double wartoscC = pow(M_E, -1.5*c) - 0.3 * pow(c, 2);
						double wartoscA = pow(M_E, -1.5*a) - 0.3 * pow(a, 2);
						if (wartoscC == 0) {
							pierw = c;
							break;
						}
						else if (wartoscA * wartoscC < 0) {
							b = c;
							continue;
						}
						else {
							a = c;
							continue;
						}
					}
					pierw = c;
					cout << "\nPierwiastek funkcji f(x) = e^(-1.5x)-0.3x^2 w przedziale <0,1> obliczony z dokladnoscia " << epsilon << " wynosi: " << pierw << endl;
					cout << "Liczba  iteracji: " << i << endl;
				}

				_getch();
				system("cls");
			}
			continue;

			case 2:
			{
				cout << "Nacisnij ENTER, aby potwierdzic zamkniecie programu...";
				_getch();
				return EXIT_SUCCESS;
			}
			break;
			}
		}
		else {
			cout << "Nie ma takiej opcji. Prosze nacisnac ENTER, aby powrocic i wybrac ponownie.";
			_getch();
			system("cls");
			continue;
		}
	}
}