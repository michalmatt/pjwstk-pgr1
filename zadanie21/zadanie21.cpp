#include <iostream>
#include <conio.h>

using namespace std;

int main() {
	while (1) {
		cout << "Witamy w programie obliczajacym iloczyn skalarny dwoch wektorow (X i Y) o maksymalnej liczbie wspolrzednych n = 10." << endl;
		cout << "Wybierz jedna z ponizszych opcji, aby kontynuowac." << endl;
		cout << "(1) Rozpocznij\n(2) Zakoncz" << endl;

		int wybor;
		cin >> wybor;

		switch (wybor) {
		case 1:
		{
			int liczbaWspolrzednychXY;
			cout << "Podaj liczbe wspolrzednych, ktora chcesz wprowadzic dla obu wektorow (limit: 10)" << endl;
			do {
				cout << "Liczba wspolrzednych dla wektorow X, Y = ";
				cin >> liczbaWspolrzednychXY;
				if (liczbaWspolrzednychXY > 10) {
					cout << "Podana liczba przekracza dozwolony limit wspolrzednych (10). Podaj ja ponownie." << endl;
				}
			} while (liczbaWspolrzednychXY > 10);

			int *wektorX = new int[liczbaWspolrzednychXY];
			cout << "Podaj kolejne wspolrzedne dla wektora X:" << endl;
			int i, n;
			for (i = 0, n = 1; n <= liczbaWspolrzednychXY; i++, n++) {
				cout << "x" << n << " = "; cin >> wektorX[i];
			}

			int *wektorY = new int[liczbaWspolrzednychXY];
			cout << "Podaj kolejne wspolrzedne dla wektora Y:" << endl;
			for (i = 0, n = 1; n <= liczbaWspolrzednychXY; i++, n++) {
				cout << "y" << n << " = "; cin >> wektorY[i];
			}

			int iloczynSkalarnyXY;
			for (i = 0, iloczynSkalarnyXY = 0; i < liczbaWspolrzednychXY; i++) {
				iloczynSkalarnyXY = iloczynSkalarnyXY + (wektorX[i] * wektorY[i]);
			}

			delete[] wektorX;
			delete[] wektorY;

			cout << "Iloczyn skalarych zadanych wektorow X i Y wynosi: " << iloczynSkalarnyXY << endl;

			_getch();
			system("cls");
		}
		continue;

		case 2:
		{
			cout << "Nacisnij ENTER, aby potwierdzic zamkniecie programu...";
			_getch();
			return EXIT_SUCCESS;
		}
		break;

		default:
		{
			cout << "Nie ma takiej opcji. Prosze nacisnac ENTER, aby powrocic i wybrac ponownie.";
			_getch();
			system("cls");
		}
		continue;
		}
	}
}