#define _USE_MATH_DEFINES
#include <iostream>
#include <conio.h>
#include <math.h>

using namespace std;

const int A = 0;
const int B = 1;

double liczWartoscFunkcji(double x) {
	double wartoscFunkcji = pow(M_E, -1.5*x) - 0.3 * pow(x, 2);
	return wartoscFunkcji;
}

void liczMetodaBisekcji(double dokladnosc) {
	double a = A;
	double b = B;
	double c;
	double pierw;
	int i;
	for (i = 0; abs(a - b) >= dokladnosc; i++) {
		c = (a + b) / 2;
		if (liczWartoscFunkcji(c) == 0) {
			pierw = c;
			break;
		}
		else if ((liczWartoscFunkcji(a) * liczWartoscFunkcji(c)) < 0) {
			b = c;
			continue;
		}
		else {
			a = c;
			continue;
		}
	}
	pierw = c;
	cout << "\nPierwiastek funkcji f(x) = e^(-1.5x)-0.3x^2 w przedziale <0,1> obliczony z dokladnoscia " << dokladnosc << " wynosi: " << pierw << endl;
	cout << "Liczba  iteracji: " << i << endl;
}

int main() {
	while (1) {
		cout << "Witamy w programie obliczajacym pierwiastek funkcji f(x) w przedziale <a,b> metoda bisekcji, gdzie:" << endl;
		cout << " f(x) = e^(-1.5x) - 0.3x^2\n a = 0;\n b = 1;" << endl;
		cout << "Wybierz jedna z ponizszych opcji, aby kontynuowac." << endl;
		cout << "(1) Rozpocznij\n(2) Zakoncz" << endl;

		int wybor;
		cin >> wybor;

		if (wybor == 1 || wybor == 2) {

			switch (wybor) {
			case 1:
			{
				double epsilon;
				cout << "Podaj dokladnosc (Epsilon) z jaka chcesz obliczyc pierwiastek.\nNp. 0.0000001" << endl;
				do {
					cout << "Epsilon = "; cin >> epsilon;
					if (epsilon < 0 || epsilon > 1) {
						cout << "Wartosc Epsilon nie moze byc mniejsza od 0 lub wieksza od 1. Podaj ja ponownie." << endl;
					}
				} while (epsilon < 0 || epsilon > 1);

				double pierw;
				if (liczWartoscFunkcji(A) == 0) {
					pierw = A;
					cout << "\nPierwiastek funkcji f(x) = e^(-1.5x)-0.3x^2 w przedziale <0,1> obliczony z dokladnoscia " << epsilon << " wynosi: " << pierw << endl;
					cout << "Liczba  iteracji: N/A" << endl;
				}
				else if (liczWartoscFunkcji(B) == 0) {
					pierw = B;
					cout << "\nPierwiastek funkcji f(x) = e^(-1.5x)-0.3x^2 w przedziale <0,1> obliczony z dokladnoscia " << epsilon << " wynosi: " << pierw << endl;
					cout << "Liczba  iteracji: N/A" << endl;
				}
				else {
					liczMetodaBisekcji(epsilon);
				}

				_getch();
				system("cls");
			}
			continue;

			case 2:
			{
				cout << "Nacisnij ENTER, aby potwierdzic zamkniecie programu...";
				_getch();
				return EXIT_SUCCESS;
			}
			break;
			}
		}
		else {
			cout << "Nie ma takiej opcji. Prosze nacisnac ENTER, aby powrocic i wybrac ponownie.";
			_getch();
			system("cls");
			continue;
		}
	}
}