#include <iostream>
#include <conio.h>

int funkcja(int arg) {
	arg += arg;
	return arg + 1;
}

int a; //zmienna globalna

using namespace std;

int main() {
	int a = 1;
	cout << 1 / 2 << endl;
	cout << 5 % 3 << endl;
	cout << a-- << endl;

	int arg = 5;
	cout << funkcja(arg) << endl;
	cout << arg << endl;

	int i;
	for (i = 0; i < 4; i++) {
		i++;
		--i;
		cout << "la";
		i++;
	}

	cout << endl;
	char Waz[3] = { 'x', 'x', 'x' };
	cout << Waz[0] << Waz[1] << Waz[2];

	cout << endl;
	int x = 0;
	x = x + 1;
	cout << "x = x+1 to: " << x << endl;

	int y = 0;

	cout << "Wartosc zmiennej globalne a to: " << a << endl;
	cout << "Wartosc modulo -1%2 to: " << -1 % 2 << endl;
	cout << "Wartosc modulo 0%1 to: " << 0 % 1 << endl;
	cout << "Wartosc modulo -4%2 to: " << -4 % 2 << endl;

	_getch();
}