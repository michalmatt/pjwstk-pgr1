#include <iostream>
#include <conio.h>
#include <math.h>
#include <algorithm>

using namespace std;

int main() {
	while (1) {
		cout << "Witamy w programie badajacym czy podana liczba (n) jest liczba doskonala. Wybierz jedna z ponizszych opcji, aby kontynuowac." << endl;
		cout << "(1) Rozpocznij\n(2) Zakoncz" << endl;

		int wybor;
		cin >> wybor;

		if (wybor == 1 || wybor == 2) {

			switch (wybor) {
			case 1:
			{
				cout << "Podaj liczbe n ponizej" << endl;
				int n;
				cout << "n = "; cin >> n;
				int Suma = 0;
				double p = round(sqrt(n));

				int i;
				for (i = 0; p > 1; i++, p--) {
					if (n % int(p) == 0) {
						Suma = Suma + p;
						int p1 = n / p;
						if (p1 != p) {
							Suma = Suma + p1;
						}
						else {
							continue;
						}
					}
					else {
						continue;
					}
				}
				Suma = Suma + 1;
				if (Suma == n) {
					cout << "Podana liczba: " << n << " jest liczba doskonala :)";
				}
				else {
					cout << "Podana liczba: " << n << " nie jest liczba doskonala :(";
				}

				_getch();
				system("cls");
			}
			continue;

			case 2:
			{
				cout << "Nacisnij ENTER, aby potwierdzic zamkniecie programu...";
				_getch();
				return EXIT_SUCCESS;
			}
			break;
			}
		}
		else {
			cout << "Nie ma takiej opcji. Prosze nacisnac ENTER, aby powrocic i wybrac ponownie.";
			_getch();
			system("cls");
			continue;
		}
	}
}