#include <iostream>
#include <conio.h>

using namespace std;

int czyLiczbaPierwsza(int x) {
	int p = 2;
	int i;
	for (i = 0; p <= x; i++, p++) {
		if (x == p) {
			return 1;
		}
		else if (x % p == 0) {
			return 0;
			break;
		}
	}
}

int main() {
	while (1) {
		cout << "Witamy w programie wyszukujacym liczby pierwsze z przedzialu [2...n] przy podanej liczbie n. Wybierz jedna z ponizszych opcji, aby kontynuowac." << endl;
		cout << "(1) Rozpocznij\n(2) Zakoncz" << endl;

		int wybor;
		cin >> wybor;

		if (wybor == 1 || wybor == 2) {

			switch (wybor) {
			case 1:
			{
				cout << "Podaj liczbe n ponizej" << endl;
				int n;
				cout << "n = "; cin >> n;
				int p = 2;
				cout << "Liczby pierwsze w przedziale [2..." << n << "] to:" << endl;
				int i;
				for (i = 0; p <= n; i++, p++) {
					if (czyLiczbaPierwsza(p) == 1) {
						cout << p << " ";
					}
				}

				_getch();
				system("cls");
			}
			continue;

			case 2:
			{
				cout << "Nacisnij ENTER, aby potwierdzic zamkniecie programu...";
				_getch();
				return EXIT_SUCCESS;
			}
			break;
			}
		}
		else {
			cout << "Nie ma takiej opcji. Prosze nacisnac ENTER, aby powrocic i wybrac ponownie.";
			_getch();
			system("cls");
			continue;
		}
	}
}