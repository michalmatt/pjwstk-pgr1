#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

int main() {
	while (1) {
		cout << "Witamy w programie sprawdzajacym czy podany wyraz jest palindromem (tj. pisany zarowno od lewej jak i prawej strony wyglada tak samo)." << endl;
		cout << "Wybierz jedna z ponizszych opcji, aby kontynuowac." << endl;
		cout << "(1) Rozpocznij\n(2) Zakoncz" << endl;

		int wybor;
		cin >> wybor;

		switch (wybor) {
		case 1:
		{
			char wpisanyWyraz[10];
			cout << "Podaj ponizej wyraz, ktory chcesz sprawdzic (limit znakow: 10)" << endl;
			do {
				cin >> wpisanyWyraz;
				if (strlen(wpisanyWyraz) > 10) {
					cout << "Podany wyraz przekracza limit znakow (10). Podaj wyraz ponownie." << endl;
				}
			} while (strlen(wpisanyWyraz) > 10);

			int dlugoscWpisanegoWyrazu = strlen(wpisanyWyraz);
			int i;
			int n;
			for (i = 0, n = 1; i < dlugoscWpisanegoWyrazu / 2; i++, n++) {
				if (wpisanyWyraz[i] == wpisanyWyraz[dlugoscWpisanegoWyrazu - n]) {
					continue;
				}
				else {
					cout << "Wyraz " << "\"" << wpisanyWyraz << "\" nie jest palindromem.";
					break;
				}
			}
			if (i == dlugoscWpisanegoWyrazu / 2) {
				cout << "Wyraz " << "\"" << wpisanyWyraz << "\" jest palindromem.";
			}

			_getch();
			system("cls");
		}
		continue;

		case 2:
		{
			cout << "Nacisnij ENTER, aby potwierdzic zamkniecie programu...";
			_getch();
			return EXIT_SUCCESS;
		}
		break;

		default:
		{
			cout << "Nie ma takiej opcji. Prosze nacisnac ENTER, aby powrocic i wybrac ponownie.";
			_getch();
			system("cls");
		}
		continue;
		}
	}
}