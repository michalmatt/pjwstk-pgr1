#include <iostream>
#include <conio.h>
#include <Windows.h>
#include <ctime>

using namespace std;

int const POCZATEK_PRZEDZIALU = 1;
int const KONIEC_PRZEDZIALU = 10;

int main() {
	while (1) {
		cout << "Witamy w programie generujacym losowa tablice dwuwymiarowa [A]8x10 skladajaca sie z elementow calkowitych z zakresu <1,10>, a nastepnie umozliwiajacym zamiane dwoch wybranych wierszy (k oraz l)." << endl;
		cout << "Wybierz jedna z ponizszych opcji, aby kontynuowac." << endl;
		cout << "(1) Rozpocznij\n(2) Zakoncz" << endl;

		int wybor;
		cin >> wybor;

		switch (wybor) {
		case 1:
		{
			int A[8][10];
			srand(time(NULL));
			int i;
			int j;
			for (i = 0; i < 8; i++) {
				for (j = 0; j < 10; j++) {
					A[i][j] = rand() % (KONIEC_PRZEDZIALU - POCZATEK_PRZEDZIALU + 1) + POCZATEK_PRZEDZIALU;
				}
			}

			cout << "Wygenerowana tablica dwuwymiarowa A:" << endl;
			int x;
			int y;
			for (x = 0; x < 8; x++) {
				for (y = 0; y < 10; y++) {
					cout << A[x][y] << " ";
				}
				cout << endl;
			}

			int k;
			int l;
			cout << "Podaj numery wierszy, ktore chcesz ze soba zamienic (k i l)" << endl;
			do {
				cout << "k = "; cin >> k;
				cout << "l = "; cin >> l;
				if (k > 8 || k < 1 || l > 8 || l < 1) {
					cout << "Niepoprawne numery wierszy (musza pochodzic z zakresu: 1-8). Podaj ja ponownie." << endl;
				}
			} while (k > 8 || k < 1 || l > 8 || l < 1);
			
			int B[10];
			int a;
			for (a = 0; a < 10; a++) {
				B[a] = A[k-1][a];
			}

			int b;
			for (b = 0; b < 10; b++) {
				A[k-1][b] = A[l-1][b];
			}

			int c;
			for (c = 0; c < 10; c++) {
				A[l-1][c] = B[c];
			}

			cout << "Tablica A po zamianie wiersza " << k << " oraz wiersza " << l << ":" << endl;
			int d;
			int e;
			for (d = 0; d < 8; d++) {
				for (e = 0; e < 10; e++) {
					cout << A[d][e] << " ";
				}
				cout << endl;
			}

			_getch();
			system("cls");
		}
		continue;

		case 2:
		{
			cout << "Nacisnij ENTER, aby potwierdzic zamkniecie programu...";
			_getch();
			return EXIT_SUCCESS;
		}
		break;

		default:
		{
			cout << "Nie ma takiej opcji. Prosze nacisnac ENTER, aby powrocic i wybrac ponownie.";
			_getch();
			system("cls");
		}
		continue;
		}
	}
}