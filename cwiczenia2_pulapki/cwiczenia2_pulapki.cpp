/*
Prosze w tybie debug przesledzic te operacje i ich rezultaty.
*/

#include <iostream>
#include <conio.h>

using namespace std;

int main() {

	int zmienna1TypuInt = 5;
	int zmienna2TypuInt = 3;
	int zmienna3TypuInt;
	char zmiennaTypuChar = 'a';
	double zmiennaTypuDouble = 0;
	bool zmiennaTypuBool = false;

	zmiennaTypuDouble = 1 / 3;
	zmiennaTypuDouble = zmienna1TypuInt / zmienna2TypuInt;
	//Zwroc uwage ze iloraz dwoch liczb calkowitych jest liczba calkowita
	//Wynik nie ma czesci ulamkowej, nawet gdy zapiszemy go w typie float lub double

	//Aby w wyniku uzyskac czesc ulamkowa, przynajmniej jeden z typow musi byc zmiennoprzecinkowy
	zmiennaTypuDouble = 1.0 / 3;
	zmiennaTypuDouble = 1 / 3.0;
	//Dlatego potrzebujemy tu rzutowania. Rzutowanie ma wyzszy priorytet niz dzielenie, ale nawiasy mozna dodac dla czytelnosci
	zmiennaTypuDouble = ((double)zmienna1TypuInt) / zmienna2TypuInt;

	//wartosci logiczne wyrazane sa wartosciami 0 - falsz oraz cokolwiek roznego od 0 - prawda; 
	//zwykle jest to 1 (zwracane np jakos wynik porownan
	zmienna3TypuInt = zmienna1TypuInt == zmienna2TypuInt;
	//istnieje rowniez typ bool (mozna przypisac mu wartosc true/false lub liczbowa, 0 zostanie zinterpretowane jako falsz)
	zmiennaTypuBool = zmienna1TypuInt == zmienna2TypuInt;

	//UWAGA NA POJEDYNCZY = ZAMIAST == 
	//takie konstukcje jak ponizej sa poprawne (wynikiem przypisania jest przypisywana wartosc)
	//1. Wartosc zmienna2TypuInt zostanie przypisana do zmienna1TypuInt. 
	//2. Wynikiem 1 jest  wartosc zmienna2TypuInt wiec ta wartosc zostanie przypisana zmienna3TypuInt
	zmienna3TypuInt = zmienna1TypuInt = zmienna2TypuInt;
	//niestety to tez nie powoduje bledu
	zmiennaTypuBool = zmienna1TypuInt = zmienna2TypuInt;

	//Uwaga na pojedynczy = rowniez w warunkach if/while
	if (zmienna1TypuInt = zmienna2TypuInt) {
		cout << "to bedzie 'prawda' jesli zmienna zmienna2TypuInt byla rozna od 0";
	}

	//inkrementacja 
	zmienna1TypuInt = 0;
	zmienna2TypuInt = zmienna1TypuInt++;
	//Jaka wartosc ma zmienna1TypuInt?
	zmienna1TypuInt = 0;
	zmienna2TypuInt = ++zmienna1TypuInt;
	//a teraz? dlaczego?

	//przypisanie liczby double do int powoduje obciecie czesci ulamkowej
	zmiennaTypuDouble = 1.1;
	zmienna1TypuInt = zmiennaTypuDouble;

	//znaki (char) to tak naprawde liczby (kody ASCI)
	zmienna1TypuInt = 'a';
	//sprawdz w tybie debug jaka wartosc ma zmiennaTypuChar; tak naprawde do int
	zmienna2TypuInt = zmiennaTypuChar;

	_getch();
}