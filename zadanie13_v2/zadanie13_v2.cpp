#include <iostream>
#include <conio.h>

using namespace std;

int liczSilnia(int x) {
	int xSilnia;
	if (x < 0) {
		return EXIT_FAILURE;
	}
	else if (x == 0) {
		xSilnia = 1;
		return xSilnia;
	}
	else {
		int i;
		for (i = 1, xSilnia = 1; i < x; i++) {
			xSilnia = xSilnia * (i + 1);
		}
		return xSilnia;
	}
}

int liczPodzbiory(int x, int y) {
	int podzbiory = liczSilnia(x) / (liczSilnia(y) * liczSilnia(x - y));
	return podzbiory;
}

int main() {
	while (1) {
		cout << "Witamy w programie obliczajacym ile roznych podzbiorow k-elementowych mozna utworzyc ze zbioru n elementow przy podanych wartosciach k oraz n. Wybierz jedna z ponizszych opcji, aby kontynuowac." << endl;
		cout << "(1) Rozpocznij\n(2) Zakoncz" << endl;

		int wybor;
		cin >> wybor;

		if (wybor == 1 || wybor == 2) {

			switch (wybor) {
			case 1:
			{
				int n, k;
				cout << "Podaj wartosci dla n oraz k ponizej." << endl;
				do {
					cout << "n = "; cin >> n;
					cout << "k = "; cin >> k;
					if (n < 0 || k < 0 || n - k < 0) {
						cout << "Wartosci n, k oraz n-k nie moga byc mniejsze od 0. Podaj je ponownie." << endl;
					}
				} while (n < 0 || k < 0 || n - k < 0);

				double m = liczPodzbiory(n,k);
				cout << "Ze zbioru " << n << " elementow mozna utworzyc " << m << " roznych podzbiorow " << k << " elementowych." << endl;

				_getch();
				system("cls");
			}
			continue;

			case 2:
			{
				cout << "Nacisnij ENTER, aby potwierdzic zamkniecie programu...";
				_getch();
				return EXIT_SUCCESS;
			}
			break;
			}
		}
		else {
			cout << "Nie ma takiej opcji. Prosze nacisnac ENTER, aby powrocic i wybrac ponownie.";
			_getch();
			system("cls");
			continue;
		}
	}
}