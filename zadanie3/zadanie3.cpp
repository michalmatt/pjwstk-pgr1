#include <iostream>
#include <conio.h>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

int main()
{
	double R, katAlfa;
	cout << "Podaj dlugosc promienia R oraz kat alfa (w stopniach) dla obliczenia pola odcnika kolowego\n";
	cout << "R = ";
	cin >> R;
	cout << "alfa = ";
	cin >> katAlfa;

	double katAlfaRadiany, poleFigury;
	katAlfaRadiany = (M_PI*katAlfa) / 180;
	poleFigury = (R*R / 2)*((M_PI*katAlfa / 180) - sin(katAlfaRadiany));

	cout << "Pole odcinka kolowego dla zadanych wartosci R oraz alfa wynosi: " << poleFigury << endl;
	_getch();
	return EXIT_SUCCESS;
}