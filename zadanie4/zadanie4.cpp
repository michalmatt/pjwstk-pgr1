#include <iostream>
#include <conio.h>

using namespace std;

int main()
{
	int a, b, c;
	cout << "Podaj 3 rozne dlugosci dla odcinkow a,b,c:\n";
	cout << "a = ";
	cin >> a;
	cout << "b = ";
	cin >> b;
	cout << "c = ";
	cin >> c;

	if (a <= 0 || b <= 0 || c <= 0) {
		cout << "Dlugosc zadnego z odcinkow nie moze byc mniejsza lub rowna 0." << endl;
		return EXIT_FAILURE;
	}

	if (a < b) {
		if (a < c) {
			cout << "Odcinek a jest najkrotszy." << endl;
			_getch();
			return EXIT_SUCCESS;
		}
		else {
			cout << "Odcinek c jest najkrotszy." << endl;
			_getch();
			return EXIT_SUCCESS;
		}
	}
	else if (b < c) {
		cout << "Odcinek b jest najkrotszy." << endl;
		_getch();
		return EXIT_SUCCESS;
	}
	else {
		cout << "Odcinek c jest najkrotszy." << endl;
		_getch();
		return EXIT_SUCCESS;
	}
}