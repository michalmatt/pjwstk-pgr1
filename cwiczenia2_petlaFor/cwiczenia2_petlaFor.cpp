#include <iostream>
#include <conio.h>

using namespace std;

int main() {
	int dlugoscA = 0;
	int dlugoscB;

	cout << "Podaj dlugosc A" << endl;
	cin >> dlugoscA;

	int i;
	for (i = 0; i<3; i++) {
		cout << "Podaj dlugosc B, rozna od A" << endl;
		cin >> dlugoscB;
		if (dlugoscA != dlugoscB) {
			break;
		}
	}

	if (dlugoscA == dlugoscB) {
		cout << "a i b wciaz sa rowne";
		_getch();
		return EXIT_FAILURE;
	}

	_getch();
	return EXIT_SUCCESS;
}