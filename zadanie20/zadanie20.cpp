#include <iostream>
#include <conio.h>

using namespace std;

int main() {
	while (1) {
		cout << "Witamy w programie znjadujacym element minimalny w zadanym przez uzytkownika ciagu liczb calkowitych." << endl;
		cout << "Wybierz jedna z ponizszych opcji, aby kontynuowac." << endl;
		cout << "(1) Rozpocznij" << endl;
		cout << "(2) Zakoncz" << endl;

		int wybor;
		cin >> wybor;

		switch (wybor) {
		case 1:
		{
			int n;
			cout << "Podaj ile liczb calkowitych chcesz wprowadzic (maksymalnie 20)." << endl;
			do {
				cin >> n;
				if (n <= 0 || n > 20) {
					cout << "Podana wartosc nie moze byc mniejsza/rowna 0 ani wieksza od 20. Sprobuj ponownie." << endl;
				}
			} while (n <= 0 || n > 20);

			cout << "Teraz podaj jakie to liczby (musza byc calkowite)." << endl;
			int *wskCiagu = new int[n];
			int i;
			int numerLiczby;
			for (i = 0, numerLiczby = 1; i < n; i++, numerLiczby++) {
				cout << "Liczba" << numerLiczby << ": "; cin >> wskCiagu[i];
			}

			int najmniejszaLiczba;
			int numerNajmniejszejLiczby;
			for (i = 0; i < n; i++) {
				if (wskCiagu[i] < wskCiagu[i + 1]) {
					najmniejszaLiczba = wskCiagu[i];
					numerNajmniejszejLiczby = i + 1;
				}
			}

			cout << "\nNajmniejsza wprowadzona liczba to: " << najmniejszaLiczba << endl;
			cout << "Numer z jakim zostala wprowadzona to: " << numerNajmniejszejLiczby << endl;

			delete[] wskCiagu;
			_getch();
			system("cls");
		}
		continue;

		case 2:
		{
			cout << "Nacisnij ENTER, aby potwierdzic zamkniecie programu...";
			_getch();
			return EXIT_SUCCESS;
		}

		default:
		{
			cout << "Nie ma takiej opcji. Prosze nacisnac ENTER, aby powrocic i wybrac ponownie.";
			_getch();
			system("cls");
			continue;
		}
		break;
		}
	}
}