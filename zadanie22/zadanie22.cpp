#include <iostream>
#include <conio.h>

using namespace std;

double liczSrednia(int tablica[], int liczbaElementowTablicy) {
	int i;
	double suma;
	for (i = 0, suma = 0; i < liczbaElementowTablicy; i++) {
		suma += tablica[i];
	}
	double srednia = suma / liczbaElementowTablicy;
	return srednia;
}

int zwrocWiekszy(int element1, int element2) {
	if (element1 > element2) {
		return element1;
	}
	else {
		return element2;
	}
}

int zwrocMniejszy(int element1, int element2) {
	if (element1 < element2) {
		return element1;
	}
	else {
		return element2;
	}
}

int main() {
	while (1) {
		cout << "Witamy w programie obliczajacym srednia arytmetyczna elementow lezacych pomiedzy elementem minimalnym a maksymalnym zadanego ciagu liczb calkowitych." << endl;
		cout << "Wybierz jedna z ponizszych opcji, aby kontynuowac." << endl;
		cout << "(1) Rozpocznij\n(2) Zakoncz" << endl;

		int wybor;
		cin >> wybor;

		switch (wybor) {
		case 1:
		{
			int n;
			cout << "Podaj ile liczb calkowitych chcesz wprowadzic (maksymalnie 20)." << endl;
			do {
				cin >> n;
				if (n <= 0 || n > 20) {
					cout << "Podana wartosc nie moze byc mniejsza/rowna 0 ani wieksza od 20. Sprobuj ponownie." << endl;
				}
			} while (n <= 0 || n > 20);

			cout << "Teraz podaj jakie to liczby (musza byc calkowite)." << endl;
			int *wskCiagu = new int[n];
			int i;
			int numerLiczby;
			for (i = 0, numerLiczby = 1; i < n; i++, numerLiczby++) {
				cout << "Liczba" << numerLiczby << ": "; cin >> wskCiagu[i];
			}

			int elementMin = wskCiagu[0];
			int indeksElementuMin = 0;
			for (i = 1; i < n; i++) {
				if (elementMin > wskCiagu[i]) {
					elementMin = wskCiagu[i];
					indeksElementuMin = i;
				}
			}

			int elementMax = wskCiagu[0];
			int indeksElementuMax = 0;
			for (i = 1; i < n; i++) {
				if (elementMax < wskCiagu[i]) {
					elementMax = wskCiagu[i];
					indeksElementuMax = i;
				}
			}

			int liczbaElementowPomiedzyMinMax = zwrocWiekszy(indeksElementuMax, indeksElementuMin) - zwrocMniejszy(indeksElementuMax, indeksElementuMin) - 1;
			int *wskCiaguMinMax = new int[liczbaElementowPomiedzyMinMax];
			for (i = 0; i < liczbaElementowPomiedzyMinMax; i++) {
				wskCiaguMinMax[i] = wskCiagu[zwrocMniejszy(indeksElementuMax, indeksElementuMin) + i + 1];
			}

			if ((liczbaElementowPomiedzyMinMax) == 0) {
				cout << "Brak elementow pomiedzy MIN (" << elementMin << ") a MAX (" << elementMax << ") dla policzenia sredniej arytmetycznej." << endl;
			}
			else {
				cout << "Srednia arytmetyczna elementow pomiedzy MIN (" << elementMin << ") a MAX (" << elementMax << ") wynosi: " << liczSrednia(wskCiaguMinMax, liczbaElementowPomiedzyMinMax) << "." << endl;
			}

			delete[] wskCiagu;
			delete[] wskCiaguMinMax;

			_getch();
			system("cls");
		}
		continue;

		case 2:
		{
			cout << "Nacisnij ENTER, aby potwierdzic zamkniecie programu...";
			_getch();
			return EXIT_SUCCESS;
		}
		break;

		default:
		{
			cout << "Nie ma takiej opcji. Prosze nacisnac ENTER, aby powrocic i wybrac ponownie.";
			_getch();
			system("cls");
		}
		continue;
		}
	}
}