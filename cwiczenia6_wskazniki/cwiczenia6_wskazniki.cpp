/*
Wskazniki - przyklady

Wiecej teorii: http://www.cplusplus.com/doc/tutorial/pointers/

*/
#include <iostream>
#include <conio.h>

using namespace std;

int main() {
	int a = 1;
	//& przed zmienna daje adres zmiennej w pamieci
	cout << "&a " << &a;
	cout << endl;

	int *wskaznikNaInt;
	//wskaznik na X to zmienna ktora przechowuje adres zmiennej typu X w pamieci
	wskaznikNaInt = &a;
	cout << "wskaznikNaInt " << wskaznikNaInt;
	cout << endl;

	//operator dereferencji
	//aby uzyc wartosci ktora jest pod adresem przechowywanym przez wskaznik nalezy dodac * przed nazwa zmiennej wskaznikowej
	cout << "*wskaznikNaInt " << *wskaznikNaInt;
	cout << endl;

	int tablica[] = { 10,20,30 };

	//nazwa tablicy jest adresem jej pierwszego elementu
	//aby przypisac do wskaznika nie potrzeba operatora pobierania adresu &
	wskaznikNaInt = tablica;
	cout << "wskaznikNaInt = tablica";
	cout << "tablica " << tablica;
	cout << endl;
	cout << "wskaznikNaInt " << wskaznikNaInt;
	cout << endl;

	//przesuwanie wskaznikow
	//wskaznik+i to adres wskaznik[i]
	cout << "*(wskaznikNaInt+1) " << *(wskaznikNaInt + 1);
	cout << endl;
	cout << "wskaznikNaInt[1] " << wskaznikNaInt[1];
	cout << endl;
	cout << "tablica [1] " << tablica[1];
	cout << endl;
	cout << "*(tablica+1) " << *(tablica + 1);
	cout << endl;

	//dynamiczne przydzielanie miejsca na wiecej elementow
	//w przeciwienstwie do klasycznej deklaracji tablicy
	//nie trzeba znac rozmiaru z gory, moze byc wyznaczany w trakcie dzialania

	cout << "Ile liczb calkowitych potrzebujesz? Przydziele Ci pamiec." << endl;
	cin >> a;
	wskaznikNaInt = new int[a];
	//new zwraca wskaznik na utworzona pamiec
	for (int i = 0; i < a; i++) {
		*(wskaznikNaInt + i) = i * i;
	}

	//moje dopisane - wyswietlenie tej tablicy wyzej
	for (int i = 0; i < a; i++) {
		cout << wskaznikNaInt[i] << " ";
	}

	_getch();
}