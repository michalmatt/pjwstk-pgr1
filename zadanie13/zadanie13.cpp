#include <iostream>
#include <conio.h>

using namespace std;

int main() {
	while (1) {
		cout << "Witamy w programie obliczajacym ile roznych podzbiorow k-elementowych mozna utworzyc ze zbioru n elementow przy podanych wartosciach k oraz n. Wybierz jedna z ponizszych opcji, aby kontynuowac." << endl;
		cout << "(1) Rozpocznij\n(2) Zakoncz" << endl;

		int wybor;
		cin >> wybor;

		if (wybor == 1 || wybor == 2) {

			switch (wybor) {
			case 1:
			{
				int n, k;
				cout << "Podaj wartosci dla n oraz k ponizej." << endl;
				do {
					cout << "n = "; cin >> n;
					cout << "k = "; cin >> k;
					if (n < 0 || k < 0 || n - k < 0) {
						cout << "Wartosci n, k oraz n-k nie moga byc mniejsze od 0. Podaj je ponownie." << endl;
					}
				} while (n < 0 || k < 0 || n - k < 0);

				int nSilnia;
				if (n == 0) {
					nSilnia = 1;
				}
				else {
					int i;
					for (i = 1, nSilnia = 1; i < n; i++) {
						nSilnia = nSilnia * (i + 1);
					}
				}

				int kSilnia;
				if (k == 0) {
					kSilnia = 1;
				}
				else {
					int j;
					for (j = 1, kSilnia = 1; j < k; j++) {
						kSilnia = kSilnia * (j + 1);
					}
				}

				int roznica = n - k;
				int roznicaSilnia;
				if (roznica == 0) {
					roznicaSilnia = 1;
				}
				else {
					int z;
					for (z = 1, roznicaSilnia = 1; z < roznica; z++) {
						roznicaSilnia = roznicaSilnia * (z + 1);
					}
				}

				double m = nSilnia / (kSilnia * roznicaSilnia);
				cout << "Ze zbioru " << n << " elementow mozna utworzyc " << m << " roznych podzbiorow " << k << " elementowych." << endl;

				_getch();
				system("cls");
			}
			continue;

			case 2:
			{
				cout << "Nacisnij ENTER, aby potwierdzic zamkniecie programu...";
				_getch();
				return EXIT_SUCCESS;
			}
			break;
			}
		}
		else {
			cout << "Nie ma takiej opcji. Prosze nacisnac ENTER, aby powrocic i wybrac ponownie.";
			_getch();
			system("cls");
			continue;
		}
	}
}