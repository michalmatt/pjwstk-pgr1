#include <iostream>
#include <conio.h>

using namespace std;

int main() {
	double a, b, c, d, e, f;
	cout << "Podaj kolejne wartosci a,b,c,d,e,f dla ukladu rownan: \n ax + by = e \n cx + dy = f \n";
	cout << "a = ";
	cin >> a;
	cout << "b = ";
	cin >> b;
	cout << "c = ";
	cin >> c;
	cout << "d = ";
	cin >> d;
	cout << "e = ";
	cin >> e;
	cout << "f = ";
	cin >> f;

	double w, wx, wy;
	w = a*d - c*b;
	wx = e*d - f*b;
	wy = a*f - c*e;

	double x, y;
	if (w != 0) {
		x = wx / w;
		y = wy / w;
	}
	else if (w == 0 && (wx != 0 || wy != 0)) {
		cout << "Uklad sprzeczny";
		_getch();
		return EXIT_FAILURE;
	}
	else {
		cout << "Nieskonczenie wiele rozwiazan";
		_getch();
		return EXIT_FAILURE;
	}
	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(3);
	cout.width(8);
	cout << "Rozwiazanie ukladu rownan: \nx = " << x << endl << "y = " << y;
	_getch();
	return EXIT_SUCCESS;
}