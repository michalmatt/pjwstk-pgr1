#include <iostream>
#include <conio.h>

using namespace std;

int main() {
	while (1) {
		cout << "Witamy w programie obliczajacym tygodniowe zarobki brutto i netto pracownika przy podanej kategorii zaszeregowania oraz liczbie przepracowanych godzin. Wybierz jedna z ponizszych opcji (poprzez wpisanie jej numeru i zatwierdzenie przyciskiem ENTER), aby kontynuowac." << endl;
		cout << "(1) Rozpocznij\n(2) Zakoncz" << endl;

		int wybor;
		cin >> wybor;

		if (wybor == 1 || wybor == 2) {

			switch (wybor) {
			case 1:
				cout << "Podaj prosze kategorie zaszeregowania (A,B,C lub D) oraz liczbe przepracowanych w tygodniu godzin." << endl;
				cout << "Kategoria zaszeregowania: ";
				char katZaszeregowania;
				cin >> katZaszeregowania;
				cout << "Liczba przepracowanych godzin w tygodniu: ";
				int liczbaGodzin;
				cin >> liczbaGodzin;

				int stawkaGodzinowa;
				switch (katZaszeregowania) {
				case 'A':
					stawkaGodzinowa = 15;
					break;
				case 'B':
					stawkaGodzinowa = 25;
					break;
				case 'C':
					stawkaGodzinowa = 30;
					break;
				case 'D':
					stawkaGodzinowa = 35;
					break;
				}

				double zarobekBrutto;
				if (liczbaGodzin > 40) {
					int liczbaNadgodzin;
					liczbaNadgodzin = liczbaGodzin - 40;
					zarobekBrutto = 40 * stawkaGodzinowa + liczbaNadgodzin * stawkaGodzinowa * 2;
				}
				else {
					zarobekBrutto = liczbaGodzin * stawkaGodzinowa;
				}

				double zarobekNetto;
				if (zarobekBrutto <= 700) {
					zarobekNetto = zarobekBrutto * 0.85;
				}
				else if (zarobekBrutto <= 1200) {
					zarobekNetto = zarobekBrutto * 0.8;
				}
				else {
					zarobekNetto = zarobekBrutto * 0.75;
				}
				cout << "\nTygodniowe zarobki pracownika wynosza:" << endl << zarobekBrutto << " zl brutto" << endl << zarobekNetto << " zl netto";
				_getch();
				system("cls");
				continue;

			case 2:
				cout << "Nacisnij ENTER, aby potwierdzic zamkniecie programu...";
				_getch();
				return EXIT_SUCCESS;
				break;
			}
		}
		else {
			cout << "Nie ma takiej opcji. Prosze nacisnac ENTER, aby powrocic i wybrac ponownie.";
			_getch();
			system("cls");
			continue;
		}
	}
}