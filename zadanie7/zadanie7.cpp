#include <iostream>
#include <conio.h>

using namespace std;

int main() {

	cout << "Witamy w programie wyznaczajacym najmniejsza wspolna wielokrotnosc dla 2 zadanych liczb calkowitych dodatnich.\nPodaj dwie liczby (a i b) calkowite dodatnie ponizej." << endl;
	cout << "a = ";
	int a;
	cin >> a;
	cout << "b = ";
	int b;
	cin >> b;

	int nnw;
	if (a > 0 && b > 0) {
		int i = 0, a1 = a, b1 = b;
		for (i; a1 != b1; i++) {
			if (a1 > b1) {
				b1 = b1 + b;
			}
			else {
				a1 = a1 + a;
			}
		}
		nnw = a1;
	}
	else {
		nnw = 0;
	}
	cout << "Najmniejsza wspolna wielokrotnosc " << a << " oraz " << b << " wynosi: " << nnw;
	_getch();
	return EXIT_SUCCESS;
}