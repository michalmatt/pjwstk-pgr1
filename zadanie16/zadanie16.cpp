#include <iostream>
#include <conio.h>
#include <Windows.h>
#include <ctime>

using namespace std;

int const MAX_LICZBA_ELEMENTOW_TABLICY_A = 100;

double liczSrednia (int tablica[], int liczbaElementowTablicy) {
	int i;
	double suma;
	for (i = 0, suma = 0; i < liczbaElementowTablicy; i++) {
		suma += tablica[i];
	}
	double srednia = suma / liczbaElementowTablicy;
	return srednia;
}

void liczWystapienia(int tablica[], int liczbaElementowTablicy) {
	int sprawdzanaLiczba;
	cout << "Podaj liczbe, dla ktorej sprawdzic ilosc wystapien: "; cin >> sprawdzanaLiczba;
	int ileWystapien = 0;
	int i;
	for (i = 0; i < liczbaElementowTablicy; i++) {
		if (tablica[i] == sprawdzanaLiczba) {
			ileWystapien++;
		}
	}
	cout << "Liczba " << sprawdzanaLiczba << " wystepuje w zadanej tablicy: " << ileWystapien << " razy." << endl;
}

void liczElementyNieparzyste(int tablica[], int liczbaElementowTablicy) {
	int sumaElementowNieparzystych = 0;
	int liczbaElementowNieparzystych = 0;
	int i;
	for (i = 0; i < liczbaElementowTablicy; i++) {
		if (tablica[i] % 2 != 0) {
			liczbaElementowNieparzystych++;
			sumaElementowNieparzystych += tablica[i];
		}
	}
	cout << "Liczba elementow nieparzystych to: " << liczbaElementowNieparzystych << ". A ich suma to: " << sumaElementowNieparzystych << "." << endl;
}

void minimum(int tablica[], int liczbaElementowTablicy) {
	int elementMinimalny = tablica[0];
	int indeksElementuMinimalnego = 0;
	int i;
	for (i = 1; i < liczbaElementowTablicy; i++) {
		if (elementMinimalny > tablica[i]) {
			elementMinimalny = tablica[i];
			indeksElementuMinimalnego = i;
		}
	}
	cout << "Element minimalny to: " << elementMinimalny << ". A jego indeks to: " << indeksElementuMinimalnego << "." << endl;
}

void maksimum(int tablica[], int liczbaElementowTablicy) {
	int elementMaksymalny = tablica[0];
	int indeksElementuMaksymalnego = 0;
	int i;
	for (i = 1; i < liczbaElementowTablicy; i++) {
		if (elementMaksymalny < tablica[i]) {
			elementMaksymalny = tablica[i];
			indeksElementuMaksymalnego = i;
		}
	}
	cout << "Element maksymalny to: " << elementMaksymalny << ". A jego indeks to: " << indeksElementuMaksymalnego << "." << endl;
}

void najwiekszaSumaPary(int tablica[], int liczbaElementowTablicy) {
	int najwiekszaSuma = tablica[0] + tablica[1];
	int element1, element2;
	int i;
	for (i = 1; i < liczbaElementowTablicy - 1; i++) {
		if (najwiekszaSuma < tablica[i] + tablica[i + 1]) {
			najwiekszaSuma = tablica[i] + tablica[i + 1];
			element1 = tablica[i];
			element2 = tablica[i + 1];
		}
	}
	cout << "Para sasiednich elementow o najwiekszej sumie (" << najwiekszaSuma << ") to: " << element1 << " i " << element2 << "." << endl;
}

void liczElementNajczestszy(int tablica[], int liczbaElementowTablicy) {
	int sprawdzanyElement, najczestszyElement;
	int ileWystapien = 0;
	int ileWystapienTmp;
	int i, j;
	for (i = 0; i < liczbaElementowTablicy; i++) {
		sprawdzanyElement = tablica[i];
		for (j = 0, ileWystapienTmp = 0; j < liczbaElementowTablicy; j++) {
			if (sprawdzanyElement == tablica[j]) {
				ileWystapienTmp++;
			}
		}
		if (ileWystapien < ileWystapienTmp) {
			ileWystapien = ileWystapienTmp;
			najczestszyElement = tablica[i];
		}
	}
	cout << "Najczestszy element (" << ileWystapien << " wystapien) to: " << najczestszyElement << "." << endl;
}

int main() {
	while (1) {
		cout << "Witamy w programie generujacym losowa tablice jednowymiarowa [A] skladajaca sie z n liczb z przedzialu <a,b> zdefiniowanego przez uzytkownika." << endl;
		cout << "Wybierz jedna z ponizszych opcji, aby kontynuowac." << endl;
		cout << "(1) Rozpocznij\n(2) Zakoncz" << endl;

		int wybor;
		cin >> wybor;

		switch (wybor) {
		case 1:
		{
			int n;
			cout << "Podaj liczbe losowych elementow (n), ktorymi zostanie wypelniona tablica [A]. Limit: n = 100." << endl;
			do {
				cout << "n = "; cin >> n;
				if (n <= 0 || n > 100) {
					cout << "Liczba elementow (n) nie moze byc mniejsza/rowna 0 lub wieksza od 100. Podaj n ponownie." << endl;
				}
			} while (n <= 0 || n > 100);

			int a, b;
			cout << "Podaj wartosci dla poczatku i konca przedzialu, z ktorego maja zostac wygenerowane liczby." << endl;
			cout << "a = "; cin >> a;
			cout << "b = "; cin >> b;

			int A[MAX_LICZBA_ELEMENTOW_TABLICY_A] = { 0 };
			srand(time(NULL));
			int i;
			for (i = 0; i < n; i++) {
				A[i] = rand() % (b - a + 1) + a;
			}

			cout << "Wygenerowana tablica [A]:" << endl;
			for (i = 0; i < n; i++) {
				cout << A[i] << " ";
			}

			cout << "\n\nKilka informacji o wygenerowanej tablicy [A]:" << endl;
			cout << "Srednia arytmetyczna elementow tablicy to: " << liczSrednia(A, n) << endl;
			liczElementyNieparzyste(A, n);
			minimum(A, n);
			maksimum(A, n);
			najwiekszaSumaPary(A, n);
			liczElementNajczestszy(A, n);
			cout << "\nDodatkowo, sprawdzmy ile razy zadana liczba wystepuje w tablicy [A]" << endl;
			liczWystapienia(A, n);

			_getch();
			system("cls");
		}
		continue;

		case 2:
		{
			cout << "Nacisnij ENTER, aby potwierdzic zamkniecie programu...";
			_getch();
			return EXIT_SUCCESS;
		}
		break;

		default:
		{
			cout << "Nie ma takiej opcji. Prosze nacisnac ENTER, aby powrocic i wybrac ponownie.";
			_getch();
			system("cls");
		}
		continue;
		}
	}
}