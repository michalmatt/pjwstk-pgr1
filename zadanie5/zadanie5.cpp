#include <iostream>
#include <conio.h>

using namespace std;

int main() {

	int n;
	cout << "Witamy w programie liczenia sumy kwadratow od 1 do zadanej liczby n.\nPodaj liczbe n ponizej." << endl;
	cout << "n = ";
	cin >> n;

	int x, i, suma;
	for (x = 1, i = 0, suma = 0; i<n; x++, i++) {
		if (i == n - 1) {
			suma = suma + x*x;
			cout << x << "*" << x;
		}
		else {
			suma = suma + x*x;
			cout << x << "*" << x << " + ";
		}
	}
	cout << " = " << suma;
	_getch();
	return EXIT_SUCCESS;
}