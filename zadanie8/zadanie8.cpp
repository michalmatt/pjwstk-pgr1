#include <iostream>
#include <conio.h>

using namespace std;

int main() {

	cout << "Witamy w programie wyznaczajacym najwiekszy wspolny podzielnik dla 2 zadanych liczb calkowitych dodatnich.\nPodaj dwie liczby (a i b) calkowite dodatnie ponizej." << endl;
	cout << "a = ";
	int a;
	cin >> a;
	cout << "b = ";
	int b;
	cin >> b;

	int nwp;
	if (a > 0 && b > 0) {
		int i = 0, a1 = a, b1 = b;
		for (i; a1 != b1; i++) {
			if (a1 > b1) {
				a1 = a1 - b1;
			}
			else {
				b1 = b1 - a1;
			}
		}
		nwp = a1;
		cout << "Najwiekszy wspolny podzielnik " << a << " oraz " << b << " wynosi: " << nwp;
		_getch();
		return EXIT_SUCCESS;
	}
	else {
		cout << "Brak rozwiazan.";
		_getch();
		return EXIT_FAILURE;
	}
}