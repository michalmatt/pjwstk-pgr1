#include <iostream>
#include <conio.h>
#include <algorithm>
#include <time.h>
#include <Windows.h>

using namespace std;

int main() {
	while (1) {
		cout << "Witamy w programie obliczajacym wielkosc Twojej wygranej na podstawie wynikow dwukrotnego rzutu kostka. Wybierz jedna z ponizszych opcji wpisujac jej numer i naciskajac ENTER:" << endl;
		cout << "(1) Wprowadze wyniki obu rzutow kostka recznie.\n(2) Chce, aby program sam wygenerowal wyniki obu rzutow kostka.\n(3) Zamknij program." << endl;

		int wybor;
		cin >> wybor;

		if (wybor == 1 || wybor == 2 || wybor == 3) {

			switch (wybor) {
			case 1:
				cout << "Podaj wynik pierwszego (p) oraz drugiego (q) rzutu kostka ponizej." << endl;
				cout << "p = ";
				int p;
				cin >> p;
				cout << "q = ";
				int q;
				cin >> q;

				int wygrana;
				if (p % 2 == 0) {
					if (q == 2 || q == 4 || q == 5) {
						wygrana = p + 3 * q;
					}
					else {
						wygrana = 2 * q;
					}
				}
				else {
					if (q == 1 || q == 3 || q == 6) {
						if (p == q) {
							wygrana = 5 * p + 3;
						}
						else {
							wygrana = 2 * q + p;
						}
					}
					else {
						if (p == 5 && q == 5) {
							wygrana = min(p, q) + 4 + 5;
						}
						else {
							wygrana = min(p, q) + 4;
						}
					}
				}
				cout << "Twoja wygrana wynosi: " << wygrana;
				_getch();
				system("cls");
				continue;

			case 2:
				cout << "Nacisnij ENTER, aby wykonac pierwszy rzut...";
				_getch();
				srand((unsigned)time(NULL));

				double p1;
				p1 = round(((double)rand() / (RAND_MAX) * 6 + 1) - 1);
				int p2;
				p2 = p1;
				cout << "\np = " << p2;
				cout << "\nNacisnij ENTER, aby wykonac drugi rzut...";
				_getch();

				double q1;
				q1 = round(((double)rand() / (RAND_MAX) * 6 + 1) - 1);
				int q2;
				q2 = q1;
				cout << "\nq = " << q2;

				int wygrana2;
				if (p2 % 2 == 0) {
					if (q2 == 2 || q2 == 4 || q2 == 5) {
						wygrana2 = p2 + 3 * q2;
					}
					else {
						wygrana2 = 2 * q2;
					}
				}
				else {
					if (q2 == 1 || q2 == 3 || q2 == 6) {
						if (p2 == q2) {
							wygrana2 = 5 * p2 + 3;
						}
						else {
							wygrana2 = 2 * q2 + p2;
						}
					}
					else {
						if (p2 == 5 && q2 == 5) {
							wygrana2 = min(p2, q2) + 4 + 5;
						}
						else {
							wygrana2 = min(p2, q2) + 4;
						}
					}
				}
				cout << "\nTwoja wygrana wynosi: " << wygrana2;
				_getch();
				system("cls");
				continue;

			case 3:
				cout << "Nacisnij ENTER, aby potwierdzic zamkniecie programu...";
				_getch();
				return EXIT_SUCCESS;
				break;
			}
		}
		else {
			cout << "Nie ma takiej opcji. Prosze nacisnac ENTER, aby powrocic i wybrac ponownie.";
			_getch();
			system("cls");
			continue;
		}
	}
}