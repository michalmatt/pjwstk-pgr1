#include <iostream>
#include <conio.h>

using namespace std;

int main()
{
	double a, b, r;
	cout << "Podaj wartosci parametrow a i b dla rownania prostej: \n y = ax + b \noraz promien r dla rownania okregu: \n x^2 + y^2 = r^2\n";
	cout << "a = ";
	cin >> a;
	cout << "b = ";
	cin >> b;
	cout << "r = ";
	cin >> r;

	double a1, b1, c1, delta;
	a1 = a*a + 1;
	b1 = 2 * a*b;
	c1 = b*b - r*r;
	delta = b1*b1 - 4 * a1*c1;

	double x1, x2, y1, y2;
	x1 = (-b1 - sqrt(delta)) / 2 * a1;
	x2 = (-b1 + sqrt(delta)) / 2 * a1;
	y1 = a*x1 + b;
	y2 = a*x2 + b;

	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(2);

	if (delta > 0) {
		cout << "Prosta przecina okrag w dwoch punktach:" << endl;
		cout << " x1 = " << x1 << "   " << " y1 = " << y1 << endl;
		cout << " x2 = " << x2 << "   " << " y2 = " << y2 << endl;
		_getch();
		return EXIT_SUCCESS;
	}
	else if (delta == 0) {
		cout << "Prosta jest styczna do okregu w punkcie:" << endl;
		cout << " x = " << x1 << "   " << " y = " << y1 << endl;
		_getch();
		return EXIT_SUCCESS;
	}
	else {
		cout << "Prosta nie ma punktow wspolnych z okregiem";
		_getch();
		return EXIT_FAILURE;
	}
}