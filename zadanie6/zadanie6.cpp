#include <iostream>
#include <conio.h>

using namespace std;

int main() {

	int n;
	cout << "Witamy w programie wyznaczajacym dzielniki dla zadanej liczby calkowitej n.\nPodaj prosze liczbe calkowita n ponizej." << endl;
	cout << "n = ";
	cin >> n;

	cout << " \nDzielniki liczby " << n << " to:" << endl;
	int i = 1;

	//Rozwiazanie z uzyciem petli for
	for (i; i <= n; i++) {
		if (n%i == 0) {
			cout << i << " ";
		}
	}

	//Rozwiazanie z uzyciem petli do...while
	/*do {
	if (n%i == 0) {
	cout << i << " ";
	}
	i++;
	} while (i <= n);*/

	//Rozwiazanie z uzyciem petli while
	/*while (i <= n) {
	if (n%i == 0) {
	cout << i << " ";
	}
	i++;
	}*/

	_getch();
	return EXIT_SUCCESS;
}